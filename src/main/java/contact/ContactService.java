package contact;

import contact.exception.ContactException;
import contact.model.Contact;

import javax.validation.constraints.NotNull;

/**
 * @author Antoine LE BORGNE
 * @version 1.0.0
 *
 * Create a contact service
 */
public class ContactService implements IContactService{

    public static final int CONSTRAINT_NAME_MAX_SIZE = 40;
    public static final int CONSTRAINT_NAME_MIN_SIZE = 3;

    IContactDAO contactDao = ContactDAO.getInstance();

    @Override
    public Contact createContact(String contactName) throws ContactException {

        if(!checkNameValidity(contactName))throw new ContactException(ContactException.FORMAT_ERR+": "+contactName);

        Contact contact = null;

        if (!contactDao.alreadyExist(contactName)){

            contact = new Contact(contactName);
            contactDao.addOne(contact);

        }else throw new ContactException(ContactException.EXIST_ERR+": "+contactName);

        //if(!contactDao.alreadyExist(contactName)) throw new ContactException(ContactException.INSERT_ERR);//check if inserted correctly

        return contact;
    }
    @Override
    public void updateContact(Contact contact, String newValue) throws ContactException {


        boolean check = ((contact == null)||  !checkNameValidity(newValue) || contactDao.alreadyExist(newValue) || !contactDao.alreadyExist(contact.getName()) || !contactDao.updateOne(contact.getName(), newValue));

        if(check)throw new ContactException(ContactException.UPDATE_ERR+": from \""+contact+"\" to \""+newValue+"\"");

    }

    @Override
    public void deleteContact(Contact contact) throws ContactException{

        if(contact == null || !contactDao.deleteOne(contact))throw new ContactException(ContactException.DELETE_ERR+": for \""+contact+"");

    }

    public boolean checkNameValidity(String value){
        return (value != null && value.trim().length() >= CONSTRAINT_NAME_MIN_SIZE && value.length() <= CONSTRAINT_NAME_MAX_SIZE && !hasSpecialChars(value));
    }

    public boolean hasSpecialChars(String value){

        for (char c: value.toCharArray()) {
            if(!(((int)c) >= 65 && ((int)c) <= 90 ) && !(((int)c) >= 97 && ((int)c) <= 122 ) )return true;
        }
        return false;
    }


}
