package contact.exception;

public class ContactException extends Exception{

    public static final String FORMAT_ERR = "contact format is not conform";
    public static final String EXIST_ERR = "contact already exist";
    public static final String INSERT_ERR = "contact has not been inserted";
    public static final String UPDATE_ERR = "contact cannot be updated";
    public static final String DELETE_ERR = "contact cannot be deleted";

    public ContactException(String errorMessage) {

        super("Err: "+errorMessage);

    }
}
