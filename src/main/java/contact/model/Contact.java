package contact.model;


import javax.validation.constraints.Size;

/**
 * @author Antoine LE BORGNE
 * @version 1.0.0
 *
 * Simple contact object
 */
public class Contact {

    private String name;

    /**
     * Default constructor of a contact
     *
     * @param name the contact name
     */
    public Contact(String name) {
        this.name = name;
    }

    /**
     * Getter of the contact name
     * @return the contact name
     */
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "name='" + name + '\'' +
                '}';
    }
}
