package contact;

import contact.exception.ContactException;
import contact.model.Contact;

import java.util.ArrayList;

public interface IContactDAO {

    boolean alreadyExist(String contactName);
    void addOne(Contact contact);
    Contact findOne(String name);
    ArrayList<Contact> findAll(String filter);
    boolean updateOne(String oldValue, String newValue);
    boolean deleteOne(Contact contact);

}
