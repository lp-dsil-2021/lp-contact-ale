package contact;

import contact.exception.ContactException;
import contact.model.Contact;

public interface IContactService {

    Contact createContact(String contactName) throws ContactException;

    void updateContact(Contact contact, String newValue) throws ContactException;

    void deleteContact(Contact contact) throws ContactException;

    boolean hasSpecialChars(String value);

}
