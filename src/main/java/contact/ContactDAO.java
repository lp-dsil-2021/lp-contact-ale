package contact;

import contact.exception.ContactException;
import contact.model.Contact;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Antoine LE BORGNE
 * @version 1.0.0
 *
 * Data Access Object to manage contact system
 */
public class ContactDAO implements IContactDAO{

    public static final ArrayList<Contact> CONTACTS = new ArrayList<>();

    private static ContactDAO contactDAO;

    public static ContactDAO getInstance(){
        if(contactDAO == null)contactDAO = new ContactDAO();
        return contactDAO;
    }

    private ContactDAO() {
    }

    /**
     * Add a single contact in the list
     *
     * @param contact the contact to add
     */
    public void addOne(Contact contact){
        CONTACTS.add(contact);
    }



    /**
     * Check if a contact is already in the list
     *
     * @param contact the contact to check
     * @return "true" if the contact already exist, "false" if not
     */
    @Override
    public boolean alreadyExist(String contact){
        return findOne(contact) != null;
    }

    /**
     * find a single contact from the list
     *
     * @param contactName the name of the contact to filter the search.
     * @return the contact if it exists, "null" if not
     */
    @Override
    public Contact findOne(String contactName){
        for(Contact c : CONTACTS)if(c.getName().equals(contactName))return c;
        //return CONTACTS.stream().anyMatch(contact -> contact.getName().equals(contactName));
        return null;
    }

    @Override
    public ArrayList<Contact> findAll(String filter) {
        return CONTACTS;
    }

    @Override
    public boolean updateOne(String oldValue, String newValue){

        Contact contact = findOne(oldValue);
        contact.setName(newValue);
        return contact.getName().equals(newValue);

    }

    @Override
    public boolean deleteOne(Contact contact) {



        return false;
    }


}
