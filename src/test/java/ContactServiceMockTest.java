import contact.ContactService;
import contact.IContactDAO;
import contact.IContactService;
import contact.exception.ContactException;
import contact.model.Contact;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.verification.VerificationMode;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceMockTest {

    public static final String LEGAL_CHARS = "AZERTYUIOPQSDFGHJKLMWXCVBNazertyuiopqsdfghjklmwxcvbn";
    public static final String ILLEGAL_CHARS = "&é(-è_çà\"#~{[|`\nèà^@]^$ù*:!;,¨£%µ/§.^`1234567890";
    public static final int TEST_AMOUNT = 200;

    @Mock
    IContactDAO contactDAO;

    @InjectMocks
    IContactService service = new ContactService();

    @Captor
    ArgumentCaptor<Contact> captor;

    @Test
    public void shouldSucceedOnClassicInsert() throws ContactException {



        Mockito.when(contactDAO.alreadyExist(Mockito.anyString())).thenReturn(false);

        for(int i = 0; i < TEST_AMOUNT; i++){

            StringBuilder builder = new StringBuilder();

            for(int j = 0;j < ((int) ((Math.random() * (40 - 3)) + 3)) ;j++)builder.append(LEGAL_CHARS.charAt((int)(Math.random() * LEGAL_CHARS.length())));

            Contact contact = service.createContact(builder.toString());

            Mockito.verify(contactDAO, Mockito.times(i+1)).addOne(captor.capture());

            Assertions.assertEquals(contact.getName(), captor.getValue().getName());


        }


    }

    @Test
    public void shouldSucceedOnInsertWith40Chars() throws ContactException {

        for(int i = 0; i < TEST_AMOUNT; i++){

            StringBuilder builder = new StringBuilder();

            for(int j = 0;j < 40 ;j++)builder.append(LEGAL_CHARS.charAt((int)(Math.random() * LEGAL_CHARS.length())));

            service.createContact(builder.toString());

        }

    }

    @Test
    public void shouldSucceedOnInsertWith3Chars() throws ContactException {

        for(int i = 0; i < TEST_AMOUNT; i++){

            StringBuilder builder = new StringBuilder();

            for(int j = 0;j < 3 ;j++)builder.append(LEGAL_CHARS.charAt((int)(Math.random() * LEGAL_CHARS.length())));

            Mockito.when(contactDAO.alreadyExist(builder.toString())).thenReturn(false);
            service.createContact(builder.toString());

        }

    }

    @Test(expected = ContactException.class)
    public void shouldFailedOnDuplicate() throws ContactException {

        Mockito.when(contactDAO.alreadyExist(Mockito.anyString())).thenReturn(true);

        for(int i = 0; i < TEST_AMOUNT; i++){

            StringBuilder builder = new StringBuilder();

            for(int j = 0;j < 40 ;j++)builder.append(LEGAL_CHARS.charAt((int)(Math.random() * LEGAL_CHARS.length())));

            service.createContact(builder.toString());

        }

    }

    @Test(expected = ContactException.class)
    public void shouldFailedWithNullName() throws ContactException {
        service.createContact(null);
    }

    @Test(expected = ContactException.class)
    public void shouldFailedWithABlankName() throws ContactException {

        for(int i = 0; i < TEST_AMOUNT; i++){

            service.createContact(" ".repeat(Math.max(0, ((int) ((Math.random() * (40 - 3)) + 3)))));

        }
    }

    @Test(expected = ContactException.class)
    public void shouldFailedWithSpecialChars() throws ContactException {

        for(int i = 0; i < TEST_AMOUNT; i++){

            StringBuilder builder = new StringBuilder();

            for(int j = 0;j < ((int) ((Math.random() * (40 - 3)) + 3)) ;j++)builder.append(ILLEGAL_CHARS.charAt((int)(Math.random() * ILLEGAL_CHARS.length())));

            service.createContact(builder.toString());

        }

    }


    @Test(expected = ContactException.class)
    public void shouldFailedWithLessThat3Chars() throws ContactException {


        for(int i = 0; i < TEST_AMOUNT; i++){

            StringBuilder builder = new StringBuilder();

            for(int j = 0;j < ((int) ((Math.random() * (2 - 1)) + 1)) ;j++)builder.append(LEGAL_CHARS.charAt((int)(Math.random() * LEGAL_CHARS.length())));

            service.createContact(builder.toString());

        }
    }

    @Test(expected = ContactException.class)
    public void shouldFailedWithMoreThat40Chars() throws ContactException {

        for(int i = 0; i < TEST_AMOUNT; i++){

            StringBuilder builder = new StringBuilder();

            for(int j = 0;j < ((int) ((Math.random() * (10000 - 41)) + 41)) ;j++)builder.append(LEGAL_CHARS.charAt((int)(Math.random() * LEGAL_CHARS.length())));

            service.createContact(builder.toString());

        }

    }

    @Test(expected = ContactException.class)
    public void shouldFailedWithUpdateWithNullName() throws ContactException {

        service.updateContact(new Contact("Robert"), null);

    }

    @Test(expected = ContactException.class)
    public void shouldFailedWithUpdateWithNullContact() throws ContactException {

        service.updateContact(null, "Michel");

    }

    @Test
    public void shouldPassWithUpdateWithNoExistingContact() throws ContactException {


        for(int i = 0; i < TEST_AMOUNT; i++){

            StringBuilder builder = new StringBuilder();

            for(int j = 0;j < ((int) ((Math.random() * (40 - 3)) + 3)) ;j++)builder.append(LEGAL_CHARS.charAt((int)(Math.random() * LEGAL_CHARS.length())));

            Mockito.when(contactDAO.alreadyExist("Phillipe")).thenReturn(true);
            Mockito.when(contactDAO.alreadyExist(builder.toString())).thenReturn(false);

            Mockito.when(contactDAO.updateOne("Phillipe", builder.toString())).thenReturn(true);

            service.updateContact(new Contact("Phillipe"),builder.toString());

        }

    }

    @Test(expected = ContactException.class)
    public void shouldFailedWithUpdateWithExistingContact() throws ContactException {

        Mockito.when(contactDAO.alreadyExist(Mockito.anyString())).thenReturn(true);

        for(int i = 0; i < TEST_AMOUNT; i++){

            StringBuilder builder = new StringBuilder();

            for(int j = 0;j < ((int) ((Math.random() * (40 - 3)) + 3)) ;j++)builder.append(LEGAL_CHARS.charAt((int)(Math.random() * LEGAL_CHARS.length())));

            service.updateContact(new Contact("Phillipe"),builder.toString());

        }

    }

}
