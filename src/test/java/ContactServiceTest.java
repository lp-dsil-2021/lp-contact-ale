import contact.ContactService;
import contact.exception.ContactException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


public class ContactServiceTest {

    ContactService contactService = new ContactService();

    @Test
    public void normalCase(){

        Assertions.assertDoesNotThrow(()-> contactService.createContact("Robert"));
        Assertions.assertDoesNotThrow(()-> contactService.createContact("Phillipe"));
        Assertions.assertDoesNotThrow(()-> contactService.createContact("Dominique"));
        Assertions.assertDoesNotThrow(()-> contactService.createContact("Michel"));

    }

    @Test
    public void limitCase(){

        Assertions.assertDoesNotThrow(()-> contactService.createContact("leo"));
        Assertions.assertDoesNotThrow(()-> contactService.createContact("azertyuiopqsdfghjklmwxcvbnazertyuiopqsdf"));

    }

    @Test
    public void errorCase(){

        Assertions.assertThrows(ContactException.class, () -> contactService.createContact("    "));
        Assertions.assertThrows(ContactException.class, () -> contactService.createContact(null));
        Assertions.assertThrows(ContactException.class, () -> contactService.createContact("&190280é"));
        Assertions.assertThrows(ContactException.class, () -> contactService.createContact("li"));
        Assertions.assertThrows(ContactException.class, () -> contactService.createContact("Robert"));
        Assertions.assertThrows(ContactException.class, () -> contactService.createContact("hjfhdjsgfjhdsfguyqetricbetfeyuhvfbgrhoszejhcfildbsqovep"));

    }


}
